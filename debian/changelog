libcache-perl (2.11-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 09 Jun 2022 21:28:57 +0100

libcache-perl (2.11-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:49:59 +0100

libcache-perl (2.11-1) unstable; urgency=low

  * Team upload.

  [ Dominic Hargreaves ]
  * Point to the team git repository

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 2.11
  * Set debhelper >= 9
  * Switch libmodule-build-perl to build dep
  * Add autopkgtest
  * Fix dependencies version with cme

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sat, 25 Jul 2015 11:24:52 -0300

libcache-perl (2.10-1) unstable; urgency=medium

  * Move package to be maintained by the Debian Perl Group
  * debian/watch: use metacpan-based URL.
  * New upstream release
  * Update Standards-Version (no changes)
  * Build-Depend on libmodule-build-perl (deprecated in core)

 -- Dominic Hargreaves <dom@earth.li>  Sat, 04 Oct 2014 19:07:05 +0100

libcache-perl (2.09-1) unstable; urgency=medium

  * New upstream release
    - drop Digest::SHA patch, integrated upstream

 -- Dominic Hargreaves <dom@earth.li>  Mon, 03 Feb 2014 00:41:49 +0000

libcache-perl (2.08-1) unstable; urgency=medium

  * New upstream release
  * Use machine-readable debian/copyright
  * Switch to dh compat level 8
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 02 Feb 2014 16:08:49 +0000

libcache-perl (2.04-3) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format
  * Add Vcs-* fields
  * Update Standards-Version (no changes)
  * Switch to minimal dh7 rules
  * Apply patch to switch from Digest::SHA1 to Digest::SHA which is
    included with perl (closes: #636648)
  * Improve description (thanks, Lintian)
  * Add explicit copyright notice to debian/copyright (thanks, Lintian)

 -- Dominic Hargreaves <dom@earth.li>  Fri, 05 Aug 2011 18:44:40 +0100

libcache-perl (2.04-2) unstable; urgency=low

  * Fix debian/rules rmdir bug (closes: 467680)
  * Update Standards-Version (no changes)
  * Standardize debian/watch format
  * debian/control: add Homepage field
  * Fix make distclean lintian warning

 -- Dominic Hargreaves <dom@earth.li>  Tue, 26 Feb 2008 21:21:14 +0000

libcache-perl (2.04-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Tue, 21 Mar 2006 14:26:37 +0000

libcache-perl (2.03-1) unstable; urgency=low

  * New upstream release.

 -- Dominic Hargreaves <dom@earth.li>  Thu, 10 Nov 2005 21:48:08 +0000

libcache-perl (2.02-1) unstable; urgency=low

  * Initial Release.

 -- Dominic Hargreaves <dom@earth.li>  Tue, 12 Jul 2005 13:36:23 +0000
